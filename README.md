# Rancher Container Configuration

This is a generic sidekick container that writes data from Rancher's
MetaData API into the specified arbitrary file.

This volume can then be mounted into any container using Docker's
volume aliasing (as of v1.9).

```yml
app:
  restart: always
  image: myorg/app
  labels:
    io.rancher.sidekicks: app-conf
  volumes_from:
    - app-conf
app-conf:
  restart: always
  image: simpletechs/rancher-container-conf:latest
```

Then configure the configuration data in `rancher-compose.yml`

```yml
app:
  metadata:
    configs:
      - content: |
          foo: bar
        path: '/config/app.conf' # note that path *SHOULD ALWAYS* be in /config so that volumes_from picks it up correctly
```

You can also provide multiple files:

```yml
app:
  restart: always
  image: myorg/app
  labels:
    io.rancher.sidekicks: app-conf
  volumes_from:
    - app-conf
app-conf:
  restart: always
  image: simpletechs/rancher-container-conf:latest
```

And configure multiple spaces in `rancher-compose.yml`

```yml
app:
  metadata:
    configs:
      - content: |
          foo: bar
        path: '/config/app/app.conf'
      - content: |
          some randome key text
        path: '/config/key/key.pem'
```
