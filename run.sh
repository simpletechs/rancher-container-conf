#!/bin/sh

mkdir -p /etc/confd/conf.d
mkdir -p /etc/confd/templates

echo "Running confd bootstrap"
/bin/confd -onetime -backend rancher -prefix /2015-07-25/self/service/metadata -confdir /etc/confd/bootstrap/

# split the produced config file @ [template]
cd /etc/confd/conf.d/
awk '/\[template\]/{x=i++;print "{{getv \"/configs/"x"/content\"}}" > "content."x".tmpl"}{print > "content."x".toml";}' contents.toml.txt
mv *.tmpl /etc/confd/templates/
rm contents.toml.txt
cd /

echo "Bootstrap complete"
echo "Starting confd"
/bin/confd -backend rancher -prefix /2015-07-25/self/service/metadata
